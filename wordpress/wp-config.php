<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'baza01');

/** Имя пользователя MySQL */
define('DB_USER', 'user01');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'User123');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'p&GGnv2_}.7ke&*lqW/Syz2deuB]$8V7#4oiHcj|SNj*$V7J`?7Tj^aDW(wNC?Qe');
define('SECURE_AUTH_KEY',  '@T&$<c)DD9G^]8*Baq-;g0?=:?PyyQ)s[+oS;nw|ux32SsCw[d=JzhJE5sXtsmHf');
define('LOGGED_IN_KEY',    'doyD31,|k$E3fT3bHYRWQXwT$%?nCQ`mFJn[gBZ8%=Opna/N~9{iymQevS6/,>t&');
define('NONCE_KEY',        '9&lTM%IK,lk}T%-,hve@K_xtTm47)_[UFJh~q`m-#;1X1x*a?.E&+hJl7/$|v/_K');
define('AUTH_SALT',        '83pz):{A)4c=*]6*DP&(B*F4G?H1Tpb6W23E9`GY}mX$TH22]. ]r$9:dO=lg&K,');
define('SECURE_AUTH_SALT', '~E%m=^!<FEFqFigC=9x f+s|I!>gQyD$uE7DCkx$m[rliM%Ksc}J/ytgKQ#l_x<%');
define('LOGGED_IN_SALT',   'NQ5+#,MF}3R7x2hxm/#sT~y6rYTE.!7ns4YwX)kAe68!;|qwg6P;.FnMHl8ggq{k');
define('NONCE_SALT',       '^TrbCV0xF-EEGMkUA`z5DI:fzbn:6#o.fUh_$[wGkuM)VrN8.M!CJz#{}#ir$&-Z');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
