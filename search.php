﻿<html>
<head>
<title>Поиск информации</title>
<body>
<style>
table {
font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
font-size: 14px;
border-radius: 10px;
border-spacing: 0;
text-align: left;
}
th {
background: #BCEBDD;
color: white;
text-shadow: 0 1px 1px #2D2020;
padding: 5px 10px;
}
th, td {
border-style: solid;
border-width: 0 1px 1px 0;
border-color: white;
}
th:first-child, td:first-child {
text-align: left;
}
th:first-child {
border-top-left-radius: 10px;
}
th:last-child {
border-top-right-radius: 10px;
border-right: none;
}
td {
padding: 5px 10px;
background: #F8E391;
}
tr:last-child td:first-child {
border-radius: 0 0 0 10px;
}
tr:last-child td:last-child {
border-radius: 0 0 10px 0;
}
tr td:last-child {
border-right: none;
}
</style>
<?php
// подключаем скрипт с ключами к БД.
require 'key.php';

// подключаемся к серверу с БД.
$link = mysqli_connect($hm, $un, $pw) or die       ('Невозможно открыть базу');

// устанавливаем кодировку при передаче данных в/из БД.
$link->set_charset("utf8");

// Выбираем БД для работы в MySQL.
$selectdb = mysqli_select_db ($link, $db);
    if (!$selectdb) {
    echo "Не удалось выбрать БД MySQL.";
    exit;
    }

// Формируем запрос из таблицы.
#$sql = "SELECT * FROM tabl01";
#$result = $link->query($sql);

$sql = 'SELECT * FROM tabl01';
$result = mysqli_query( $link, $sql)  or die ('Запрос не удался:' .mysqli_error());

// В цикле перебираем все записи таблицы и выводим их.
echo '<table>';
echo '<tr><th>ID     </th><th>ФИО     </th><th>Адрес     </th><th>Телефон     </th><th>Станция</th></tr>';
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
{
// Выводим данные в виде структурированной таблицы.
echo '<tr><td>'.$row['id'].'</td><td>'.$row['name'].'</td><td>'.$row['adres'].'</td><td>'.$row['phone'].'</td><td>'.$row['station'].'</td></tr>';

#echo '<tr><td>ID: '.$row['id'].'</td><td>ФИО: '.$row['name'].'</td><td>Адрес: '.$row['adres'].'</td><td>Тел: '.$row['phone'].'</td></tr>';
#echo '<th><td>'.$row['id'].'</td></th>';
#echo '<td><th> '.$row['name'].'</th></td>';
#echo '<td> '.$row['adres'].'</td>';
#echo '<td> '.$row['phone'].' </td>';

}
echo '</table>';

// Освобождаем память от результата.
mysqli_free_result($result);

// Закрываем соединение.
mysqli_close($link);
?>
  </body>
</body>
</head>
</html>